package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class CarListPage extends AbstractPage{

    private String filterBtn = "//span[@class='filter-toggle']";
    private String preOwnedBtn = "//span[text()=' Pre-Owned']/..";
    private String filterColour = "//div[contains(@class, 'category-filter')]/p[text()='Colour']";
    private String filterColourList = filterColour + "/../..//div[@data-test-id='multi-select-dropdown']";
    private String colourBtn = "//a[text()=' BRILLANTBLUE ']";
    private String carCardList = "//div[@class='dcp-cars-srp-results__tile']";
    private String sortingBtn = "//wb-select/*[text()='Sorting']/../select";

    public CarListPage(WebDriver driver) {
        super(driver);
    }

    public CarListPage filterBtnClick() {
        click(filterBtn);
        return this;
    }

    public CarListPage preOwnedBtnClick() {
        if (!driver.findElement(By.xpath(preOwnedBtn)).isDisplayed())
            filterBtnClick();
        click(preOwnedBtn);
        return this;
    }

    public CarListPage chooseColour() {
        var colourFilter = driver.findElement(By.xpath(filterColour));
        var colourListElement = driver.findElement(By.xpath(filterColourList));
        var js = (JavascriptExecutor) driver;

        //waitLoader(); for firefox
        if (!driver.findElement(By.xpath(preOwnedBtn)).isDisplayed())
            filterBtnClick();
        js.executeScript("arguments[0].scrollIntoView(true);", colourFilter);
        colourFilter.click();
        wait.until(c -> colourListElement.isEnabled());
        colourListElement.click();
        var colourBtnElement = driver.findElement(By.xpath(colourBtn));
        js.executeScript("document.body.style.zoom='0.8'");
        js.executeScript("arguments[0].click();", colourBtnElement);
        return this;
    }

    public CarPage selectMostExpensiveCar() {
        var selectElement = driver.findElement(By.xpath(sortingBtn));
        var select = new Select(selectElement);
        select.selectByVisibleText(" Price (descending) ");
        var firstCarElement = driver.findElements(By.xpath(carCardList)).get(0);
        waitLoader();
        firstCarElement.click();
        return new CarPage(driver);
    }
}
