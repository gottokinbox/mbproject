package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class LocationPopup extends AbstractPage {

    private String selectStateElement = "//wb-select/label[contains(text(),'state')]/../select";
    private String postCodeElement = "//wb-input/label[contains(text(),'Postal Code')]/../input";
    private String radioButtonPrivate = "//wb-radio-control//span[text()='Private']/../div";
    private String submitBtn = "//button[@data-test-id='state-selected-modal__close']";
    private String shadowRootTagName = "cmm-cookie-banner";

    public LocationPopup(WebDriver driver) {
        super(driver);
    }

    public LocationPopup open(String url) {
        driver.get(url);
        return this;
    }

    public LocationPopup agreeToAllCookies() {
        var rootElement = driver.findElement(By.tagName(shadowRootTagName));
        var shadowBtn = rootElement.getShadowRoot().findElements(By.cssSelector("wb7-button")).get(1);
        var agreeBtn = shadowBtn.getShadowRoot().findElement(By.cssSelector("button"));
        wait.until(d-> agreeBtn.isDisplayed());
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();", agreeBtn);
        return this;
    }

    public LocationPopup selectItemFromSelector(String value) {
        var selectElement = driver.findElement(By.xpath(selectStateElement));
        var select = new Select(selectElement);
        wait.until(d-> selectElement.isEnabled());
        select.selectByVisibleText(value);
        return this;
    }

    public LocationPopup sendPostCode(String postCode) {
        driver.findElement(By.xpath(postCodeElement)).sendKeys(postCode);
        return this;
    }

    public LocationPopup selectPrivateRadioBtn() {
        driver.findElement(By.xpath(radioButtonPrivate)).click();
        return this;
    }

    public CarListPage submit() {
        driver.findElement(By.xpath(submitBtn)).click();
        return new CarListPage(driver);
    }
}
