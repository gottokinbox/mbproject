package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class ContactDetailsPopup extends AbstractPage {


    private String firstNameField = "//label[text()=' First Name ']/../input";
    private String lastNameField = "//label[text()=' Last Name ']/../input";
    private String emailField = "//label[text()=' E-Mail ']/../input";
    private String phoneField = "//label[text()=' Phone ']/../input";
    private String postalCodeField = "//label[text()=' Postal Code ']/../input";
    private String errorBlock = "//div[@class='dcp-error-message']";
    private String proceedBtn = "//button[text()=' Proceed ']";
    private String emailError = "//input[@inputmode='email']/../../wb-control-error";

    public ContactDetailsPopup(WebDriver driver) {
        super(driver);
    }

    public ContactDetailsPopup fillPopupWithInvalidData(String firstName, String lastName, String email, String phone, String postalCode) {
        fillField(firstNameField, firstName);
        fillField(lastNameField, lastName);
        fillField(emailField, email);
        fillField(phoneField, phone);
        fillField(postalCodeField, postalCode);
        var proceedBtnElement = driver.findElement(By.xpath(proceedBtn));
        var js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", proceedBtnElement);
        js.executeScript("arguments[0].click();", proceedBtnElement);
        return this;
    }

    public String getGeneralError() {
        var error = driver.findElement(By.xpath(errorBlock)).getText();
        return error;
    }

    public String getEmailError() {
        var error = driver.findElement(By.xpath(emailError)).getText();
        return error;
    }
}
