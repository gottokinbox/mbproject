package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;

public abstract class AbstractPage {

    protected WebDriver driver;
    protected Wait<WebDriver> wait;
    protected String loader = "//div[@class='dcp-loader']";


    protected AbstractPage(WebDriver driver) {
        this.driver = driver;
        wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofMillis(300))
                .ignoring(ElementNotInteractableException.class);
    }

    protected void fillField(String xpath, String value) {
        var element = driver.findElement(By.xpath(xpath));
        element.clear();
        element.sendKeys(value);
    }

    protected void click(String xpath) {
        var element = driver.findElement(By.xpath(xpath));
        wait.until(c -> element.isEnabled());
        element.click();
    }

    protected void waitLoader() {
        var loaderElement = driver.findElement(By.xpath(loader));
        wait.until(c->loaderElement.isDisplayed());
        wait.until(c->!loaderElement.isDisplayed());
    }
}
