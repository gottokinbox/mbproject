package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.io.FileWriter;
import java.io.IOException;

public class CarPage extends AbstractPage{

    private String modelYear = "(//span[@class='dcp-vehicle-details-list-item__value'])[3]"; //some problem here, that's why the xpath is shit
    private String vinNumber = "//span[text()='VIN ']/../span[2]";
    private String enquireBtn = "//button[text()=' Enquire Now ']";

    public CarPage(WebDriver driver) {
        super(driver);
    }

    public CarPage saveYearAndVinToFile() {
        var info = driver.findElement(By.xpath(modelYear)).getText() + "\n" + driver.findElement(By.xpath(vinNumber)).getText();
        try (FileWriter writer = new FileWriter("output.txt")) {
            writer.write(info);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this;
    }

    public ContactDetailsPopup enquireBtnClick() {
        var enquireBtnElement = driver.findElement(By.xpath(enquireBtn));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", enquireBtnElement);
        return new ContactDetailsPopup(driver);
    }
}
