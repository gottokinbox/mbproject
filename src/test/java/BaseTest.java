import Pages.LocationPopup;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;

import java.time.Duration;

public class BaseTest {

    private final static String URL = "https://shop.mercedes-benz.com/en-au/shop/vehicle/srp/demo";

    protected static WebDriver driver;
    protected static LocationPopup locationPopup;

    @BeforeAll
    public static void setUp()
    {
        driver = WebDriverFactory.create(System.getProperty("browser").trim().toLowerCase());
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(90));
    }

    protected static LocationPopup openMainPage() {
        locationPopup = new LocationPopup(driver);
        locationPopup.open(URL);
        return locationPopup;
    }

    @AfterAll
    public static void close()
    {
        if (driver != null)
            driver.quit();
    }
}
