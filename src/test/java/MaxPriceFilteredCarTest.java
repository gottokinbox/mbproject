import Pages.CarListPage;
import Pages.CarPage;
import Pages.ContactDetailsPopup;
import Pages.LocationPopup;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MaxPriceFilteredCarTest extends BaseTest{

    @Test
    public void validateDataErrorTest() {

        LocationPopup locationPopup = openMainPage()
                .agreeToAllCookies()
                .selectItemFromSelector("New South Wales")
                .sendPostCode("2007")
                .selectPrivateRadioBtn();

        CarListPage carListPage = locationPopup.submit();
        carListPage.preOwnedBtnClick()
                .chooseColour();
        CarPage carPage = carListPage.selectMostExpensiveCar();
        carPage.saveYearAndVinToFile();

        ContactDetailsPopup contactDetailsPopup = carPage.enquireBtnClick();
        contactDetailsPopup.fillPopupWithInvalidData("Daria", "Burtovaia", "doodoo.rt", "0449893993", "2007");

        Assertions.assertEquals("An error has occurred. Please check the following sections: Please check the data you entered.",
                contactDetailsPopup.getGeneralError().replace('\n', ' '));
        Assertions.assertEquals( "Please enter a valid email address using a minimum of six characters.", contactDetailsPopup.getEmailError());
    }
}
