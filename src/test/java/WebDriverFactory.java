import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;


public class WebDriverFactory {

    public static WebDriver create(String browser) {
            switch (browser) {
                case "firefox":
                    WebDriverManager.firefoxdriver().setup();
                    FirefoxOptions ffOptions = new FirefoxOptions().addArguments("--headless");
                    return new FirefoxDriver(ffOptions);
                case "chrome":
                    WebDriverManager.chromedriver().setup();
                    ChromeOptions chOptions = new ChromeOptions().addArguments("--headless")
                            .addArguments("--no-sandbox")
                            .addArguments("--disable-dev-shm-usage");
                    return new ChromeDriver(chOptions);
                case "safari":
                    WebDriverManager.safaridriver().setup();
                    SafariOptions safOptions = new SafariOptions();
                    safOptions.setCapability("headless", true);
                    return new SafariDriver(safOptions);
                default:
                    System.out.println("browser is not found");
                    return null;
            }
    }
}
