# MBProject



## Getting started
Task 1 is in task_1.pdf

Task 2 is in src/test/java

Task 3 is in .gitlab-ci.yml and it's flaky (I don't have enough time to handle it).
***
To launch the project you need:
- Java 16
***
Please, use the following command to launch the test:
```
mvn clean test -Dbrowser=chrome

browser can be 'chrome', 'firefox' or 'safari'
```